#include "browserwindows.h"

#include <QWebSecurityOrigin>

browserWindows::browserWindows(QWidget *parent,int id_c,QString url_c,MainWindow *win) :
    QWidget(parent)
{
    id=id_c;
    start_url=url_c;

    inspector=0;

    center = new QGridLayout;
    view=new QWebView;
    main_window=win;



    center->setMargin(0);
    center->addWidget(view);

    footer = new QHBoxLayout;
    progressBar=new QProgressBar;
    progressBar->setMaximumHeight(20);
    progressBar->setMaximumWidth(100);
    le_url=new QLineEdit;
    connect(le_url,SIGNAL(returnPressed()),SLOT(le_url_returnPressed()));

    footer->setMargin(0);
    footer->addWidget(le_url);
    footer->addWidget(progressBar);

    footer_inspector = new QHBoxLayout;

    mainLayout = new QVBoxLayout(this);
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    mainLayout->addLayout(center);
    mainLayout->addLayout(footer);
    mainLayout->addLayout(footer_inspector);

    pageLoadTimeOut=new QTimer;
    timeLoadPage=new QTime;
    connect(pageLoadTimeOut, SIGNAL(timeout()), view, SLOT(stop()));

    javaScriptEnabled=true;
    view->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(view,SIGNAL(customContextMenuRequested(const QPoint&)),SLOT(webViewContextMenu(const QPoint&)));

    connect(view,SIGNAL(loadStarted()),this,SLOT(webViewLoadStarded()));
    connect(view,SIGNAL(loadProgress(int)),this,SLOT(webViewLoadProgress(int)));
    connect(view,SIGNAL(loadFinished(bool)),this,SLOT(webViewLoadFinished(bool)));

    loadSettings();
    this->setWindowIcon(view->icon());
}

void browserWindows::populateJavaScriptWindowObject()
{
    /*MyApi *js_object = new MyApi(this);
    view->page()->mainFrame()->addToJavaScriptWindowObject(js_object->objectName(), js_object);*/
}

void browserWindows::le_url_returnPressed()
{
    if (le_url->text().contains("http://"))
    {
       view->load(QUrl(le_url->text()));
    }else{
        le_url->setText("http://"+le_url->text());
        view->load(QUrl(le_url->text()));
    }
}

void browserWindows::javaScriptEnabledDisabled()
{
    if (javaScriptEnabled)
    {
        javaScriptEnabled=false;
        view->settings()->setAttribute(QWebSettings::JavascriptEnabled,false);
    }
    else
    {
        javaScriptEnabled=true;
        view->settings()->setAttribute(QWebSettings::JavascriptEnabled,true);
        view->reload();
    }
}


void browserWindows::webViewContextMenu(const QPoint &pos)
{

    QWebHitTestResult r = view->page()->mainFrame()->hitTestContent(pos);
    QMenu *contextMenu=new QMenu;
    if (r.isContentEditable ())
    {
        contextMenu->addAction (view->pageAction (QWebPage::Paste));
    }
    if (r.isContentSelected ())
    {
        contextMenu->addAction (view->pageAction (QWebPage::Copy));

    }
    contextMenu->addSeparator ();
    contextMenu->addAction (view->pageAction (QWebPage::Forward));
    contextMenu->addAction (view->pageAction (QWebPage::Back));
    contextMenu->addAction (view->pageAction (QWebPage::Stop));
    contextMenu->addAction (view->pageAction (QWebPage::Reload));
    contextMenu->addSeparator ();
    if (javaScriptEnabled)
    {
        QAction * jsEnabled = new QAction(QIcon(":/image/images/tick_64.png"),"javaScript Enabled",this);
        contextMenu->addAction(jsEnabled);
        connect(jsEnabled, SIGNAL(triggered()), this, SLOT(javaScriptEnabledDisabled()));
    }
    else
    {
        QAction * jsDisabled = new QAction(QIcon(":/image/images/delete_64.png"),"javaScript Disabled",this);
        contextMenu->addAction(jsDisabled);
        connect(jsDisabled, SIGNAL(triggered()), this, SLOT(javaScriptEnabledDisabled()));
    }
    QAction * settings = new QAction(QIcon(":/image/images/settings_64.png"),"settings",this);
    contextMenu->addAction(settings);
    connect(settings, SIGNAL(triggered()), this, SLOT(OpenSettings_clicked()));

    QAction * reload_Script = new QAction (QIcon(":/image/images/arrow_down_64.png"),"reload Script",this);
    contextMenu->addAction(reload_Script);
    connect(reload_Script, SIGNAL(triggered()), this, SLOT(ReloadScript_clicked()));

    QAction * savePage = new QAction (QIcon(":/image/images/save_64.png"),"save page",this);
    contextMenu->addAction(savePage);
    connect(savePage, SIGNAL(triggered()), this, SLOT(savePage()));


    QAction * web_inspector = new QAction (QIcon(":/image/images/firebug.png"),"web inspector",this);
    contextMenu->addAction(web_inspector);
    connect(web_inspector, SIGNAL(triggered()), this, SLOT(firebugActivated()));

    contextMenu->exec(view->mapToGlobal(pos));
}

void browserWindows::firebugActivated ()
{

    if (inspector==0){
        inspector = new QWebInspector;
        inspector->setPage(view->page());
        footer_inspector->addWidget(inspector);
        qDebug() << inspector->isActiveWindow();
    }else{
        delete inspector;
        inspector=0;
    }
}

void browserWindows::webViewLoadStarded()
{
    pageLoadTimeOut->start(timeOut);
}

void browserWindows::OpenSettings_clicked()
{
    settings * form = new settings(0,id);
    form->setWindowModality(Qt::ApplicationModal);
    form->show();
}

void browserWindows::ReloadScript_clicked()
{
    javaScriptEnabled=true;
    loadSettings();
    view->reload();
}

void browserWindows::savePage()
{
    QString pageHTML=view->page()->currentFrame()->toHtml();
    QString file_dialog = QFileDialog::getSaveFileName(this,"SaveHTML");
    QFile file(file_dialog);
    file.open(QFile::WriteOnly);
    QTextStream ts(&file);
    ts << pageHTML;  // QWebView webview;
    file.close();
}

void browserWindows::webViewLoadFinished(bool finish)
{
    if (finish){
        timeLoadPage->restart ();
        pageLoadTimeOut->stop();
        QSettings settings("ujsb2","webView_"+QString::number(id));
        QStringList iskat=settings.value("iskat").toString().split("[SPRTR]");
        QStringList zamenit=settings.value("zamenit").toString().split("[SPRTR]");
        if (QFile::exists(settings.value("scriptUrl").toString()))
        {
            QFile file(settings.value("scriptUrl").toString());

            if (file.open(QIODevice::ReadOnly))
            {
               QTextStream text(&file);
               //userJs=decodeStr(text.readAll());
               userJs=text.readAll();
               for (int i=0;i<iskat.length();i++)
               {
                   userJs.replace(iskat[i],zamenit[i]);
               }
            }
            file.close();
        }
        QString resultScriptArr=view->page()->currentFrame()->evaluateJavaScript(userJs).toString();
    }
}

void browserWindows::loadSettings()
{
    QSettings settings("ujsb2","webView_"+QString::number(id));
    start_url=settings.value("firstUrl").toString();
    QString userAgent=settings.value("userAgent").toString();
    QStringList iskat=settings.value("iskat").toString().split("[SPRTR]");
    QStringList zamenit=settings.value("zamenit").toString().split("[SPRTR]");
    timeOut=settings.value("timeOut").toInt();
    if (timeOut<=0)
    {
        timeOut=30000;
    }
    userJs="";

    if (QFile::exists(settings.value("scriptUrl").toString()))
    {
        QFile file(settings.value("scriptUrl").toString());

        if (file.open(QIODevice::ReadOnly))
        {
           QTextStream text(&file);
           userJs=text.readAll();
           for (int i=0;i<iskat.length();i++)
           {
               userJs.replace(iskat[i],zamenit[i]);
           }
        }
        file.close();
    }
    TabName=settings.value("tabName").toString();

    webPage * page = new webPage(userAgent);
    view->setPage((QWebPage*)page);

    MyApi *myapi = new MyApi( this );
    myapi->setWebView( view );
    myapi->setMainWindow(main_window);

    if (settings.value("proxy").toBool())
    {
        QNetworkProxy proxy;
        proxy.setType(QNetworkProxy::HttpProxy);
        proxy.setHostName(settings.value("proxyServer").toString());
        proxy.setPort(settings.value("proxyPort").toInt());
        proxy.setUser(settings.value("proxyLogin").toString());
        proxy.setPassword(settings.value("proxyPassword").toString());
        view->page()->networkAccessManager()->setProxy(proxy);

    }

    view->settings()->setAttribute(QWebSettings::AutoLoadImages,settings.value("loadPictures").toBool());
    view->settings()->setAttribute(QWebSettings::LocalStorageEnabled,true);
    view->settings()->setAttribute(QWebSettings::XSSAuditingEnabled,false);
    view->settings()->setAttribute(QWebSettings::LocalContentCanAccessFileUrls,true);
    view->settings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls,true);
    view->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled,true);
    view->settings()->setLocalStoragePath("webview");

    QWebSecurityOrigin::removeLocalScheme("qrc");

    view->load(QUrl(start_url));
}

void browserWindows::webViewLoadProgress(int key)
{
    this->setWindowTitle(TabName/*+" "+view->title()*/);
    progressBar->setValue(key);
    if (key<=15)
    {
        //view->page()->mainFrame()->evaluateJavaScript("var navigator = new Object; navigator.appName = '"+wv_appName+"';navigator.userAgent= '"+wv_userAgent+"';");
    }
    le_url->setText(view->url().toString());
    le_url->setCursorPosition(0);
}
