#ifndef BROWSERWINDOWS_H
#define BROWSERWINDOWS_H

#include <QWidget>
#include <QWebView>
#include <QDebug>
#include <QMessageBox>
#include <QLineEdit>
#include <QProgressBar>
#include <QAction>
#include <QMenu>
#include <QWebFrame>
#include <QNetworkProxy>
#include <QCloseEvent>
#include <QTimer>
#include <QNetworkDiskCache>
#include <QApplication>
#include <QWebInspector>
#include "../settings/settings.h"
#include "../webPage/browserControl.h"

#include <QGridLayout>

#include "testobject.h"
#include "../mainwindow.h"


class browserWindows : public QWidget
{
    Q_OBJECT
public:
    explicit browserWindows(QWidget *parent = 0,int id=0,QString start_url="about:blank",MainWindow *win=0);
    QLineEdit *le_url;
    QProgressBar *progressBar;
    QStatusBar *statusBar;
    QWebView *view;

    QGridLayout *center;
    QHBoxLayout *footer;
    QVBoxLayout *mainLayout;
    QHBoxLayout *footer_inspector;

    QTimer *pageLoadTimeOut;
    QTime *timeLoadPage;
    bool javaScriptEnabled;
    int id;
    int timeOut;
    QString start_url;
    QMessageBox message;
    QString userJs;
    QString TabName;
    QString lastCaptchaID;
    QString lastCaptchaBase64;

public slots:
    void firebugActivated();
    void javaScriptEnabledDisabled();
    void loadSettings();
    void le_url_returnPressed();
    void OpenSettings_clicked();
    void ReloadScript_clicked();
    void savePage();
    void webViewLoadFinished(bool);
    void webViewLoadStarded();
    void webViewLoadProgress(int);
    void webViewContextMenu(const QPoint&);
    void populateJavaScriptWindowObject();

signals:
        void setTrayIconText(QString title,QString body);
        void playWaw(QString wavUrl);

private:
        MainWindow *main_window;
        QWebInspector *inspector;
protected:

    void closeEvent( QCloseEvent *e )
    {
        // здесь код по минимизации, а потом:
        //e -> ignore();
    }

};

#endif // BROWSERWINDOWS_H
