#include <qdebug.h>
#include <qwebview.h>
#include <qwebframe.h>
#include "QMainWindow"
#include "../mainwindow.h"



#include "testobject.h"

MyApi::MyApi( QObject *parent )
    : QObject( parent )
{
    //qDebug() << "start MyApi";

}

void MyApi::setWebView( QWebView *view )
{
    QWebPage *page = view->page();
    frame = page->mainFrame();

    attachObject();
    connect( frame, SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(attachObject()) );
}

void MyApi::attachObject()
{
    frame->addToJavaScriptWindowObject( QString("MyApi"), this );
}

void MyApi::sendTrayMessage(const QString &title, const QString &body){
    main_window->setTextTrayICon(title,body);
}

QString MyApi::getImageBase64(){
    QWebElementCollection col = frame->findAllElements("img");
    QString ret="{";
    int i=0;
    QStringList list;
    foreach (QWebElement el, col) {
        list << "\""+QString::number(i)+"\":\""+getBase64Image(el)+"\"";
        i++;
    }
    ret+=list.join(", ")+"}";
    return ret;
}

QString MyApi::getBase64Image(QWebElement el){
    QImage image(el.geometry().width(), el.geometry().height(), QImage::Format_ARGB32);
    QPainter painter(&image);
    el.render(&painter);
    painter.end();
    QByteArray byteArray;
    QBuffer buffer(&byteArray);
    image.save(&buffer, "PNG");
    return QString::fromLatin1(byteArray.toBase64().data());
}

QString MyApi::getImageBase64(int id){
    QWebElementCollection col = frame->findAllElements("img");
    int i=0;
    foreach (QWebElement el, col) {
        if (i==id){
            QImage image(el.geometry().width(), el.geometry().height(), QImage::Format_ARGB32);
            QPainter painter(&image);
            el.render(&painter);
            painter.end();

            QByteArray byteArray;
            QBuffer buffer(&byteArray);
            image.save(&buffer, "PNG"); // writes the image in PNG format inside the buffer
            QString iconBase64 = QString::fromLatin1(byteArray.toBase64().data());
            return iconBase64;
        }
        i++;
    }
    return "";
}

QString MyApi::sendRequestGet(const QString &url)
{
    QUrl url_r=QUrl(url);
    QNetworkRequest request(url_r);
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("User-Agent", "browser_agent");

    QNetworkAccessManager * pManager = new QNetworkAccessManager;
    QEventLoop loop;
    QNetworkReply *reply = pManager->get(request);
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));

    loop.exec();

    return QString::fromUtf8(reply->readAll());
}

QString MyApi::sendRequestPost(const QString &url, const QString &data)
{
    QUrl url_r=QUrl(url);
    QByteArray data_r=data.toUtf8();
    QNetworkRequest request(url_r);
    request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    request.setRawHeader("User-Agent", "browser_agent");

    QNetworkAccessManager * pManager = new QNetworkAccessManager;
    QEventLoop loop;
    QNetworkReply *reply = pManager->post(request, data_r);
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));

    loop.exec();

    return QString::fromUtf8(reply->readAll());
}
