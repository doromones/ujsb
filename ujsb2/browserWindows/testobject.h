#ifndef TESTOBJECT_H
#define TESTOBJECT_H

#include <qobject.h>
#include <QNetworkRequest>
#include <QEventLoop>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QUrl>

#include <QWebElement>
#include <QBuffer>

#include "../mainwindow.h"


class QWebView;
class QWebFrame;

class MyApi : public QObject
{
    Q_OBJECT
public:
    MyApi( QObject *parent );

    void setWebView( QWebView *view );
    void setMainWindow(MainWindow *win){
        main_window=win;
    }

public slots:
    QString sendRequestGet(const QString &url);
    QString sendRequestPost(const QString &url,const QString &data);
    QString getImageBase64();
    QString getImageBase64(int id);
    void sendTrayMessage(const QString &title,const QString &body);

private slots:
    void attachObject();
    QString getBase64Image(QWebElement);

private:
    MainWindow *main_window;
    QWebFrame *frame;
};

#endif // TESTOBJECT_H

