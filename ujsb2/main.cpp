#include <QtWidgets>
#include "mainwindow.h"
#include <QProcess>
#include "dsingleapplication/dsingleapplication.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed (false);
    a.setOrganizationName ("ujsb2");
    a.setApplicationName ("Ujsb2");
    a.setApplicationVersion ("2_6");

    DSingleApplication instance( "UJSB2" );

    if ( instance.isRunning() ) {
        instance.sendMessage( "SHOW" );
        return 0;
    }else{
      MainWindow w;
      w.show();
      QObject::connect( &instance, SIGNAL( messageReceived(const QString &) ),
                            &w,   SLOT( instanceMessageReceived(const QString &) ) );
      return a.exec();
    }
}
