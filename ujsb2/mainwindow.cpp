#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "browserWindows/browserwindows.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    setGeometryFromSettings();
    createTrayIcon();
    subWindowCount=0;
    addNewSubWindow();
    initToolBar();
}

void MainWindow::addNewSubWindow()
{
    browserWindows *brswind= new browserWindows(this,subWindowCount++,"about:blank",this);
    connect(brswind,SIGNAL(setTrayIconText(QString,QString)),this,SLOT(setTextTrayICon(QString,QString)));
    connect(brswind,SIGNAL(playWaw(QString)),this,SLOT(playWav(QString)));
    connect(brswind,SIGNAL(destroyed()),this,SLOT(determinationSubWindowCount()));
    qDebug() << subWindowCount;
    ui->mdiArea->addSubWindow(brswind);
    brswind->show ();
}

void MainWindow::initToolBar()
{
    QPushButton *btn=new QPushButton();
    btn->setIcon(QIcon(":/image/images/plus_64.png"));
    connect(btn,SIGNAL(clicked()),this,SLOT(addNewSubWindow()));
    ui->toolBar->addWidget(btn);
}

void MainWindow::setGeometryFromSettings ()
{
    QSettings settings("ujsb2","config");
    restoreGeometry(settings.value("geometry").toByteArray());
    if ((settings.value ("width").toInt ()!=0)&&(settings.value ("height").toInt ()!=0))
    {
        resize (settings.value ("width").toInt (),settings.value ("height").toInt ());
    }
}

void MainWindow::moveEvent (QMoveEvent *)
{
    QSettings settings("ujsb2","config");
    settings.setValue("geometry", saveGeometry());
}

void MainWindow::resizeEvent (QResizeEvent *__e)
{
    if ((width ()!=456)||(height ()!=346))
    {
        QSettings settings("ujsb2","config");
        settings.setValue ("width",width ());
        settings.setValue ("height",height ());
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setTextTrayICon(QString Title, QString Body)
{
    trayIcon->showMessage(Title,Body,QSystemTrayIcon::Information,10000000);
}

void MainWindow::playWav(QString WavUrl)
{
  QSound::play(WavUrl);
}

void MainWindow::createTrayIcon()
{
    QAction *minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    QAction *maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

    QAction *restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    QAction *quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));


    QMenu *trayIconMenu = new QMenu(this);

    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), SLOT(iconTrayActivated(QSystemTrayIcon::ActivationReason)));
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setIcon(QIcon(":/image/images/network_64.png"));
    trayIcon->show();
    this->hide ();

}

void MainWindow::iconTrayActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick)
    {
        if (isHidden())
        {
            setGeometryFromSettings ();
            showNormal();
        }
        else
        {
            hide();
        }
    }
}

void MainWindow::on_actionCascade_triggered()
{
    ui->mdiArea->cascadeSubWindows();
}

void MainWindow::on_actionTileVertically_triggered()
{
    QList<QMdiSubWindow *> subwindowlist = ui->mdiArea->subWindowList();
        if (subwindowlist.count() < 2) {
            ui->mdiArea->tileSubWindows();
            return;
        }
        int wWidth = width() / subwindowlist.count();
        int x = 0;
        foreach (QMdiSubWindow *pSubWindow, subwindowlist)
        {
            pSubWindow->resize(wWidth, height());
            pSubWindow->move(x, 0);
            x += wWidth;
        }
}

void MainWindow::on_actionTileHorizontally_triggered()
{
    QList<QMdiSubWindow *> subwindowlist = ui->mdiArea->subWindowList();
        if (subwindowlist.count() < 2) {
            ui->mdiArea->tileSubWindows();
            return;
        }
        int wHeight = height() / subwindowlist.count();
        int y = 0;
        foreach (QMdiSubWindow *pSubWindow, subwindowlist)
        {
            pSubWindow->resize(width(), wHeight);
            pSubWindow->move(0, y);
            y += wHeight;
        }
}

void MainWindow::on_actionTile_triggered()
{
    ui->mdiArea->tileSubWindows();
}
