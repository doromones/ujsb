#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QMainWindow>
#include <QWebView>
#include <QTimer>
#include <QMessageBox>
#include <QSystemTrayIcon>
#include <QMdiSubWindow>
#include <QSound>
#include <QProcess>
#include <QCryptographicHash>
#include <QPushButton>
#include <QIcon>


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QWebView *webviewGetData;
    QWebView *testTime;
    QWebView *testVersion;

    QStringList date;

    QSystemTrayIcon *trayIcon;

    QTimer *pageLoadTimeOutDATA;
    QTimer *pageLoadTimeOutTestTime;
    QTimer *pageLoadTimeOutTestVersion;

    int subWindowCount;

public slots:
    void moveEvent (QMoveEvent *);
    void setGeometryFromSettings();
    void resizeEvent (QResizeEvent *__e);
    void setTextTrayICon(QString Title,QString Body);
    void playWav(QString WavUrl);

    void initToolBar();

    void determinationSubWindowCount(){
        --subWindowCount;
    }

    void addNewSubWindow();

    void createTrayIcon();
    void iconTrayActivated(QSystemTrayIcon::ActivationReason reason);

private:
    Ui::MainWindow *ui;
    bool trial;

private slots:
    void on_actionTile_triggered();
    void on_actionTileHorizontally_triggered();
    void on_actionTileVertically_triggered();
    void on_actionCascade_triggered();


public slots:

signals:

};

#endif // MAINWINDOW_H
