#include "settings.h"
#include "ui_settings.h"
#include <QWebFrame>
//using namespace CleverAlloyNetwork;

settings::settings(QWidget *parent,int id) :
    QWidget(parent),
    ui(new Ui::settings)
{
    ui->setupUi(this);
    settingsName=QString::number(id);
    loadSettings();
}

settings::~settings()
{
    delete ui;
}


void settings::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void settings::on_listWidget_clicked(QModelIndex index)
{
    ui->listWidget_2->setCurrentRow(index.row());
    ui->listWidget_2->editItem(ui->listWidget_2->item(index.row()));
}

void settings::on_listWidget_2_clicked(QModelIndex index)
{
    ui->listWidget->setCurrentRow(index.row());
    ui->listWidget->editItem(ui->listWidget->item(index.row()));
}

void settings::on_button_Add_clicked()
{
    if (ui->lineEdit->text().length()!=0)
    {
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
        item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
        item->setText(ui->lineEdit->text());
        QListWidgetItem *item2 = new QListWidgetItem(ui->listWidget_2);
        item2->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
        item2->setText(ui->lineEdit_2->text());
    }
    else
    {
        QMessageBox message;
        message.setText("please enter text line menu");
        message.exec();
    }
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit->setFocus();
}

void settings::on_button_Del_clicked()
{
    ui->listWidget->takeItem(ui->listWidget->currentIndex().row());
    ui->listWidget_2->takeItem(ui->listWidget_2->currentIndex().row());
}

void settings::on_button_Save_clicked()
{

    QString file_dialog = QFileDialog::getSaveFileName(this,"SaveFile");
    QSettings settings(file_dialog, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    QString iskat="";
    QString zamenit="";
    for (int i=0;i<ui->listWidget->count();i++)
    {
        if (i==ui->listWidget->count()-1)
        {
            iskat=iskat+ui->listWidget->item(i)->text();
        }
        else {
            iskat=iskat+ui->listWidget->item(i)->text()+"[SPRTR]";
        }
    }
    for (int i=0;i<ui->listWidget_2->count();i++)
    {
        if (i==ui->listWidget_2->count()-1)
        {
            zamenit=zamenit+ui->listWidget_2->item(i)->text();
        }
        else {
            zamenit=zamenit+ui->listWidget_2->item(i)->text()+"[SPRTR]";
        }
    }

    settings.setValue("iskat",iskat);
    settings.setValue("zamenit",zamenit);
    settings.setValue("scriptUrl",ui->lineEdit_ScriptUrl->text());
    settings.setValue("firstUrl",ui->lineEdit_firstUrl->text());
    settings.setValue("tabName",ui->lineEdit_tabName->text());
    settings.setValue("loadPictures",ui->checkBox_loadPictures->isChecked());
    settings.setValue("userAgent",ui->le_userAgent->text());
    settings.setValue("proxy",ui->gb_proxy->isChecked());
    settings.setValue("proxyServer",ui->le_proxyServer->text());
    settings.setValue("proxyPort",ui->le_proxyPort->text());
    settings.setValue("proxyLogin",ui->le_proxyLogin->text());
    settings.setValue("proxyPassword",ui->le_proxyPassword->text());
    settings.setValue("timeOut",ui->sb_timeOut->text());
}

void settings::on_button_Load_clicked()
{
    QString file_dialog = QFileDialog::getOpenFileName(this,"load ini file","","ini file *.ini (*.ini)");
    if (file_dialog!="")
    {
        QSettings settings(file_dialog, QSettings::IniFormat);
        settings.setIniCodec("UTF-8");

        ui->lineEdit_ScriptUrl->setText(settings.value("scriptUrl").toString());
        ui->lineEdit_firstUrl->setText(settings.value("firstUrl").toString());
        ui->lineEdit_tabName->setText(settings.value("tabName").toString());
        ui->checkBox_loadPictures->setChecked(settings.value("loadPictures").toBool());
        ui->le_userAgent->setText(settings.value("userAgent").toString());
        ui->gb_proxy->setChecked(settings.value("proxy").toBool());
        ui->le_proxyServer->setText(settings.value("proxyServer").toString());
        ui->le_proxyPort->setText(settings.value("proxyPort").toString());
        ui->le_proxyLogin->setText(settings.value("proxyLogin").toString());
        ui->le_proxyPassword->setText(settings.value("proxyPassword").toString());
        ui->sb_timeOut->setValue(settings.value("timeOut").toInt());

        QString iskat="";
        QString zamenit="";
        iskat=settings.value("iskat").toString();
        zamenit=settings.value("zamenit").toString();

        QStringList zamenit_=zamenit.split("[SPRTR]");
        QStringList iskat_=iskat.split("[SPRTR]");

//        QString str;

        ui->listWidget->clear();
        ui->listWidget_2->clear();
        foreach (QString str, iskat_)
        {
                QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
                item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
                item->setText(str);

        }
        foreach (QString str, zamenit_)
        {
                QListWidgetItem *item = new QListWidgetItem(ui->listWidget_2);
                item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
                item->setText(str);
        }

    }

}

void settings::on_button_Closed_clicked()
{
    this->close();
}

void settings::on_button_OpenScript_clicked()
{
    QString file_dialog = QFileDialog::getOpenFileName(this);
    ui->lineEdit_ScriptUrl->setText(file_dialog);
}

void settings::on_button_SaveAll_clicked()
{
    QSettings settings("ujsb2","webView_"+settingsName);
    QString iskat="";
    QString zamenit="";
    for (int i=0;i<ui->listWidget->count();i++)
    {
        if (i==ui->listWidget->count()-1)
        {
            iskat=iskat+ui->listWidget->item(i)->text();
        }
        else {
            iskat=iskat+ui->listWidget->item(i)->text()+"[SPRTR]";
        }
    }
    for (int i=0;i<ui->listWidget_2->count();i++)
    {
        if (i==ui->listWidget_2->count()-1)
        {
            zamenit=zamenit+ui->listWidget_2->item(i)->text();
        }
        else {
            zamenit=zamenit+ui->listWidget_2->item(i)->text()+"[SPRTR]";
        }
    }

    settings.setValue("iskat",iskat);
    settings.setValue("zamenit",zamenit);
    settings.setValue("scriptUrl",ui->lineEdit_ScriptUrl->text());
    settings.setValue("firstUrl",ui->lineEdit_firstUrl->text());
    settings.setValue("tabName",ui->lineEdit_tabName->text());
    settings.setValue("loadPictures",ui->checkBox_loadPictures->isChecked());
    settings.setValue("userAgent",ui->le_userAgent->text());
    settings.setValue("proxy",ui->gb_proxy->isChecked());
    settings.setValue("proxyServer",ui->le_proxyServer->text());
    settings.setValue("proxyPort",ui->le_proxyPort->text());
    settings.setValue("proxyLogin",ui->le_proxyLogin->text());
    settings.setValue("proxyPassword",ui->le_proxyPassword->text());
    settings.setValue("timeOut",ui->sb_timeOut->text());

    this->close();
}

void settings::loadSettings()
{

    settings::move(250,100);
    QSettings settings("ujsb2","webView_"+settingsName);

    setWindowTitle(settings.value("tabName").toString());
    ui->lineEdit_ScriptUrl->setText(settings.value("scriptUrl").toString());
    ui->lineEdit_firstUrl->setText(settings.value("firstUrl").toString());
    ui->lineEdit_tabName->setText(settings.value("tabName").toString());
    ui->checkBox_loadPictures->setChecked(settings.value("loadPictures").toBool());
    ui->le_userAgent->setText(settings.value("userAgent").toString());
    ui->gb_proxy->setChecked(settings.value("proxy").toBool());
    ui->le_proxyServer->setText(settings.value("proxyServer").toString());
    ui->le_proxyPort->setText(settings.value("proxyPort").toString());
    ui->le_proxyLogin->setText(settings.value("proxyLogin").toString());
    ui->le_proxyPassword->setText(settings.value("proxyPassword").toString());
    ui->sb_timeOut->setValue(settings.value("timeOut").toInt());

    QString iskat="";
    QString zamenit="";
    iskat=settings.value("iskat").toString();
    zamenit=settings.value("zamenit").toString();
    QStringList zamenit_=zamenit.split("[SPRTR]");
    QStringList iskat_=iskat.split("[SPRTR]");

    QString str;
    foreach (str, iskat_)
    {
            QListWidgetItem *item = new QListWidgetItem(ui->listWidget);
            item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
            item->setText(str);
     }
    foreach (str, zamenit_)
    {
            QListWidgetItem *item = new QListWidgetItem(ui->listWidget_2);
            item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
            item->setText(str);
    }
}
