#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>
#include <QMessageBox>
#include <QSettings>
#include <QModelIndex>
#include <QFileDialog>
#include <QDebug>
#include "../mainwindow.h"
#include <QTime>
#include <QNetworkProxy>


namespace Ui {
    class settings;
}

class settings : public QWidget {
    Q_OBJECT
public:
    settings(QWidget *parent = 0,int id=0);
    ~settings();
    QString settingsName;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::settings *ui;


private slots:
    void loadSettings();
    void on_button_SaveAll_clicked();
    void on_button_OpenScript_clicked();
    void on_button_Closed_clicked();
    void on_button_Load_clicked();
    void on_button_Save_clicked();
    void on_button_Del_clicked();
    void on_button_Add_clicked();
    void on_listWidget_2_clicked(QModelIndex index);
    void on_listWidget_clicked(QModelIndex index);
};

#endif // SETTINGS_H
