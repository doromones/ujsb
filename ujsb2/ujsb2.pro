#-------------------------------------------------
#
# Project created by QtCreator 2010-09-27T15:15:12
#
#-------------------------------------------------

QT       += network webkitwidgets widgets

TARGET = ujsb2
TEMPLATE = app


CONFIG(debug, debug|release) {
     DEFINES = __MY_DEBUG_DEFINE__
    DESTDIR = build

    unix:TARGET = bin/debug/unix/$$QT_ARCH/ujsb2
    macx:TARGET = build/bin/debug/macx/ujsb2
    win32:TARGET = bin/debug/win32/ujsb2
    win64:TARGET = bin/debug/win64/ujsb2

    unix:OBJECTS_DIR = build/debug/o/unix
    macx:OBJECTS_DIR = build/debug/o/macx
    win32:OBJECTS_DIR = build/debug/o/win32
    win64:OBJECTS_DIR = build/debug/o/win64

    unix:MOC_DIR = build/debug/moc/unix
    macx:MOC_DIR = build/debug/moc/macx
    win32:MOC_DIR = build/debug/moc/win32
    win64:MOC_DIR = build/debug/moc/win64

    unix:UI_DIR = build/debug/ui/unix
    macx:UI_DIR = build/debug/ui/macx
    win32:UI_DIR = build/debug/ui/win32
    win64:UI_DIR = build/debug/ui/win64

    unix:RCC_DIR = build/debug/rcc/unix
    macx:RCC_DIR = build/debug/rcc/macx
    win32:RCC_DIR = build/debug/rcc/win32
    win64:RCC_DIR = build/debug/rcc/win64
}
CONFIG(release, debug|release) {
    DEFINES = __MY_RELEASE_DEFINE__
    DESTDIR = build
    unix:TARGET = bin/release/unix/$$QT_ARCH/ujsb2
    macx:TARGET = build/bin/release/macx/ujsb2
    win32:TARGET = bin/release/win32/ujsb2
    win64:TARGET = bin/release/win64/ujsb2

    unix:OBJECTS_DIR = build/release/o/unix
    macx:OBJECTS_DIR = build/release/o/macx
    win32:OBJECTS_DIR = build/release/o/win32
    win64:OBJECTS_DIR = build/release/o/win64

    unix:MOC_DIR = build/release/moc/unix
    macx:MOC_DIR = build/release/moc/macx
    win32:MOC_DIR = build/release/moc/win32
    win64:MOC_DIR = build/release/moc/win64

    unix:UI_DIR = build/release/ui/unix
    macx:UI_DIR = build/release/ui/macx
    win32:UI_DIR = build/release/ui/win32
    win64:UI_DIR = build/release/ui/win64

    unix:RCC_DIR = build/release/rcc/unix
    macx:RCC_DIR = build/release/rcc/macx
    win32:RCC_DIR = build/release/rcc/win32
    win64:RCC_DIR = build/release/rcc/win64
}

SOURCES += main.cpp\
        mainwindow.cpp \
    browserWindows/browserwindows.cpp \
    webPage/browserControl.cpp \
    settings/settings.cpp \
    dsingleapplication/dsingleapplication.cpp \
    browserWindows/testobject.cpp



HEADERS  += mainwindow.h \
    browserWindows/browserwindows.h \
    webPage/browserControl.h \
    settings/settings.h \
    dsingleapplication/dsingleapplication.h \
    browserWindows/testobject.h

FORMS    += mainwindow.ui \
    settings/settings.ui \
    textCoder/textcoder.ui

RESOURCES += resources/resources.qrc
RC_FILE = resources/icon/myicon.rc

OTHER_FILES += \
    qxtxmlrpc/qxtxmlrpc.pro \
    resources/images/firebug.png \
    resources/js/jquery-1.7.2.min.js \
    qjson/README
