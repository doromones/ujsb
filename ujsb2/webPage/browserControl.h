#ifndef BROWSERCONTROL_H
#define BROWSERCONTROL_H
#include <QWebPage>
#include <QWebView>
#include <QContextMenuEvent>
#include <QWebHitTestResult>
#include <QDebug>

class webPage : public QWebPage {
        public:
            webPage(QString);
            QString userAgent;
        QString userAgentForUrl(const QUrl &url ) const;
};
class browserControl : public QWebView {
        public: browserControl();
};
#endif // BROWSERCONTROL_H
